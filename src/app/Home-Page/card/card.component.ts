import { NgForOf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import data from "../../data";


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  arr: any[] = data;

}
