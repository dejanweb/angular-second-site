import { Component, OnInit } from '@angular/core';


declare var $: JQueryStatic;


@Component({
  selector: 'app-section2',
  templateUrl: './section2.component.html',
  styleUrls: ['./section2.component.scss']
})
export class Section2Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const person = <HTMLElement>document.querySelector(".carousel-h5");

    const p1 = <HTMLElement>document.querySelector(".p1");
    const p2 = <HTMLElement>document.querySelector(".p2");
    const p3 = <HTMLElement>document.querySelector(".p3");
    const p4 = <HTMLElement>document.querySelector(".p4");
    const p5 = <HTMLElement>document.querySelector(".p5");

    const i1 = <HTMLElement>document.querySelector(".i1");
    const i2 = <HTMLElement>document.querySelector(".i2");
    const i3 = <HTMLElement>document.querySelector(".i3");
    const i4 = <HTMLElement>document.querySelector(".i4");
    const i5 = <HTMLElement>document.querySelector(".i5");

    p1.classList.add("activ");


    // $("#carouselExampleCaptions").on("slid.bs.carousel", function (e) {
    //   document.querySelectorAll(".carousel-indicators").forEach((e) => {
    //     e.classList.add("visibility");
    //   });
      
    // })
      
    $('.carousel-control-next-icon').on('click',function(){
        let slideFrom = $("#carouselExampleCaptions").children("div:nth-child(2)").find(".active").index();
    
        if(slideFrom === 0) {
            person.innerText = "Richard Harris";
            p5.classList.remove("activ");
            p2.classList.add("activ");
            p1.classList.remove("activ");
        }
        if(slideFrom === 1) {
            person.innerText = "Andy McIntyre";
            p2.classList.remove("activ");
            p3.classList.add("activ");
        }
        if(slideFrom === 2) {
            person.innerText = "John Barrows";
            p3.classList.remove("activ");
            p4.classList.add("activ");
        }
        if(slideFrom === 3) {
            person.innerText = "Morgan Ingram";
            p4.classList.remove("activ");
            p5.classList.add("activ");
        }
        if(slideFrom === 4) {
            person.innerText = "James Buckley";
            p5.classList.remove("activ")
            p1.classList.add("activ");
        }
    });
    
    $('.carousel-control-prev-icon').on('click',function(){
        let slideFrom = $("#carouselExampleCaptions").children("div:nth-child(2)").find(".active").index();
    
        if(slideFrom === 0) {
            person.innerText = "Morgan Ingram";
            p1.classList.remove("activ");
            p5.classList.add("activ");
        }
        if(slideFrom === 1) {
            person.innerText = "James Buckley";
            p2.classList.remove("activ");
            p1.classList.add("activ");
        }
        if(slideFrom === 2) {
            person.innerText = "Richard Harris";
            p3.classList.remove("activ");
            p2.classList.add("activ");
        }
        if(slideFrom === 3) {
            person.innerText = "Andy McIntyre";
            p4.classList.remove("activ");
            p3.classList.add("activ");
        }
        if(slideFrom === 4) {
            person.innerText = "John Barrows";
            p5.classList.remove("activ");
            p4.classList.add("activ");
        }
    });
        


    
i1.addEventListener("click", () => {
  person.innerText = "James Buckley";
    p1.classList.add("activ");
    p5.classList.remove("activ");
    p4.classList.remove("activ");
    p3.classList.remove("activ");
    p2.classList.remove("activ");
})
i2.addEventListener("click", () => {
  person.innerText = "Richard Harris";
    p2.classList.add("activ");
    p1.classList.remove("activ");
    p4.classList.remove("activ");
    p3.classList.remove("activ");
    p5.classList.remove("activ");
})
i3.addEventListener("click", () => {
  person.innerText = "Andy Mclntyre";
    p3.classList.add("activ");
    p5.classList.remove("activ");
    p4.classList.remove("activ");
    p1.classList.remove("activ");
    p2.classList.remove("activ");
})
i4.addEventListener("click", () => {
  person.innerText = "John Barrows";
    p4.classList.add("activ");
    p5.classList.remove("activ");
    p1.classList.remove("activ");
    p3.classList.remove("activ");
    p2.classList.remove("activ");
})
i5.addEventListener("click", () => {
  person.innerText = "Morgan Ingram";
    p5.classList.add("activ");
    p1.classList.remove("activ");
    p4.classList.remove("activ");
    p3.classList.remove("activ");
    p2.classList.remove("activ");
})



    
    $('#carouselExampleCaptions .carousel-item').each(function(){
        var next = $(this).next();
        if (!next.length) {
          next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));
        
        for (var i=0;i<1;i++) {
          next=next.next();
          if (!next.length) {
              next = $(this).siblings(':first');
            }
          
          next.children(':first-child').clone().appendTo($(this));
        }
      });



    
    
    
    // const items = document.querySelectorAll('.carousel .carousel-item')
    
    // items.forEach((el) => {
    //   const minPerSlide = 3;
    //   let next = el.nextElementSibling
    //   for (var i=1; i<minPerSlide; i++) {
    //       if (!next) {
              
    //         next = items[0]
    //       }
    //       let cloneChild = next.cloneNode(true)
    //       el.appendChild(cloneChild.children[0])
    //       next = next.nextElementSibling
    //   }
    // })
    
  }
  
}
