import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainResponsiveComponent } from './main-responsive.component';

describe('MainResponsiveComponent', () => {
  let component: MainResponsiveComponent;
  let fixture: ComponentFixture<MainResponsiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainResponsiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainResponsiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
