import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first-section',
  templateUrl: './first-section.component.html',
  styleUrls: ['./first-section.component.scss']
})
export class FirstSectionComponent implements OnInit {

  constructor() { }

  
  ngOnInit(): void {
    const firstBtn = <HTMLElement>document.querySelector(".view-detail-btn");
    const secondBtn = <HTMLElement>document.querySelector(".view-detail-btn2");
    const price1 = <HTMLElement>document.getElementById("price1");
    const price2 = <HTMLElement>document.getElementById("price2");
    const time = <HTMLElement>document.querySelector("#time");

    price1.innerHTML = "With edit - $1500 <br> - 2-3 minute highlight video";
    price2.innerHTML = "Capture only - $1000 <br> - RAW Footage delivered <br> <br>";
    
    secondBtn.addEventListener("click", () => {
        firstBtn.classList.remove("view-detail-btn");
        secondBtn.classList.remove("view-detail-btn2");
        firstBtn.classList.add("view-detail-btn2");
        secondBtn.classList.add("view-detail-btn");
    
        secondBtn.style.marginRight = "0px";
        firstBtn.style.marginRight = "20px";
    
        price1.innerHTML = "With edit - $2500 <br> - 3-5 minute highlight video";
        price2.innerHTML = "Capture only - $1800 <br> - RAW Footage delivered <br> <br>";
    })


    firstBtn.addEventListener("click", () => {
        firstBtn.classList.remove("view-detail-btn2");
        secondBtn.classList.remove("view-detail-btn");
        firstBtn.classList.add("view-detail-btn");
        secondBtn.classList.add("view-detail-btn2");
    
        price1.innerHTML = "With edit - $1500 <br> - 2-3 minute highlight video";
        price2.innerHTML = "Capture only - $1000 <br> - RAW Footage delivered <br> <br>";
    })


    let exampleModal = <HTMLElement>document.getElementById('viewDetailsModal');

    exampleModal.addEventListener('show.bs.modal', function (event:any) {
      let button = event.relatedTarget
      let recipient:any = button.getAttribute('data-bs-whatever')
    })

    $('#viewDetailsModal').on('shown.bs.modal', function (e) {
      $('body').removeClass('modal-open');
      $('body').removeAttr("style");
    })


  }

}
