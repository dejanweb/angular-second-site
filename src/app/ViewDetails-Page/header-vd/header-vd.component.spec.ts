import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderVDComponent } from './header-vd.component';

describe('HeaderVDComponent', () => {
  let component: HeaderVDComponent;
  let fixture: ComponentFixture<HeaderVDComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderVDComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderVDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
