import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainthreeComponent } from './mainthree.component';

describe('MainthreeComponent', () => {
  let component: MainthreeComponent;
  let fixture: ComponentFixture<MainthreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainthreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainthreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
