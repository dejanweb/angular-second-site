import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponentComponent } from './Home-Page/header-component/header-component.component';
import { SectionComponent } from './Home-Page/section/section.component';
import { Section2Component } from './Home-Page/section2/section2.component';
import * as $ from 'jquery';
import { SectionGridComponent } from './Home-Page/section-grid/section-grid.component';
import { BottomSectionComponent } from './Home-Page/bottom-section/bottom-section.component';
import { FooterSectionComponent } from './Home-Page/footer-section/footer-section.component';
import { NavComponent } from '../app/nav/nav.component';
import { CardComponent } from './Home-Page/card/card.component';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './Contact-Page/header/header.component';
import { MainComponent } from './Home-Page/main/main.component';
import { MiddleSectionComponent } from './Contact-Page/middle-section/middle-section.component';
import { MainComponentTwo } from './Contact-Page/main/main.component';
import { FormComponent } from './Contact-Page/form/form.component';
import { FooterContactViewComponent } from '../app/footer-contact-viewdetails/footer-contact.component';
import { MainthreeComponent } from './ViewDetails-Page/mainthree/mainthree.component';
import { HeaderVDComponent } from './ViewDetails-Page/header-vd/header-vd.component';
import { CoverComponent } from './ViewDetails-Page/cover/cover.component';
import { FirstSectionComponent } from './ViewDetails-Page/first-section/first-section.component';
import { MainResponsiveComponent } from './OurPackages-ResponsivePage/main-responsive/main-responsive.component';
import { CoverPhotoSectionComponent } from './OurPackages-ResponsivePage/cover-photo-section/cover-photo-section.component';
import { parseHTML } from 'jquery';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponentComponent,
    SectionComponent,
    Section2Component,
    SectionGridComponent,
    BottomSectionComponent,
    FooterSectionComponent,
    NavComponent,
    CardComponent,
    HeaderComponent,
    MainComponent,
    MiddleSectionComponent,
    MainComponentTwo,
    FormComponent,
    FooterContactViewComponent,
    MainthreeComponent,
    HeaderVDComponent,
    MainthreeComponent,
    HeaderVDComponent,
    CoverComponent,
    FirstSectionComponent,
    MainResponsiveComponent,
    CoverPhotoSectionComponent,

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: "", component: MainComponent},
      {path: "", redirectTo: "../app/Home-Page/main", pathMatch: "full"},
      
      {path: "Contact-Page", component: MainComponentTwo},
      // {path: "", redirectTo: "../app/Contact-Page/main", pathMatch: "full"},
      
      {path: "ViewDetails-Page", component: MainthreeComponent},
      // {path: "", redirectTo: "../app/ViewDetails-Page/mainthree", pathMatch: "full"},
      
      {path: "OurPackages-ResponsivePage", component: MainResponsiveComponent},
      // {path: "", redirectTo: "../app/OurPackages-ResponsivePage/main-responsive", pathMatch: "full"},
      {path: "**", redirectTo: "", pathMatch: "full"},
    ]),
    AppRoutingModule,
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})


export class AppModule { 
}
