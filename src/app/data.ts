export default [
 {
     id: 0,
     img: "../assets/slike/first.png",
     titleBig: "1 Videographer Package",
     titleSmall: "PERFECT FOR:",
     li: " ipsum dolor sit amet",
     btn: "VIEW DETAILS"
 },
 {
    id: 1,
    img: "../assets/slike/second.png",
    titleBig: "2 Videographer Package",
    titleSmall: "PERFECT FOR:",
    li: " ipsum dolor sit amet",
    btn: "VIEW DETAILS"
},
{
    id: 2,
    img: "../assets/slike/third.png",
    titleBig: "Breakout Session Package",
    titleSmall: "PERFECT FOR:",
    li: " ipsum dolor sit amet",
    btn: "VIEW DETAILS"
},
{
    id: 3,
    img: "../assets/slike/fourth.jpg",
    titleBig: "Keynote Package",
    titleSmall: "PERFECT FOR:",
    li: " ipsum dolor sit amet",
    btn: "VIEW DETAILS"
}
]