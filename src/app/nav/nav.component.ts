import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SectionGridComponent } from '../Home-Page/section-grid/section-grid.component';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit(): void {
    $(".navbar-toggler").on("click", () => {
      $("body, html").css("overflow-y", "hidden");
    })

    $(".menu-close, .b1, .b2").on("click", () => {
      $("body, html").css("overflow-y", "auto");
    })
  }

}
